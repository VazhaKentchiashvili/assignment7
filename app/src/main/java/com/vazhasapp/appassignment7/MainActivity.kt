package com.vazhasapp.appassignment7

import android.content.res.Resources
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.vazhasapp.appassignment7.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), RecyclerAdapter.VazhaClicker {

    private lateinit var binding: ActivityMainBinding
    private val monstersDummyData = mutableListOf<MonsterModel>()
    private lateinit var myAdapter: RecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setTheme(R.style.Theme_APPAssignment7)
        setContentView(binding.root)

        fillDummyData()
        init()
    }

    private fun init() {
        myAdapter = RecyclerAdapter(monstersDummyData, this)

        binding.myRecyclerView.apply {
            adapter = myAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
        }
        myAdapter.notifyDataSetChanged()
    }

    override fun vazhaClick(position: Int, currentMonster: MonsterModel) {
        Toast.makeText(this, "შენ უჯიკე ${currentMonster.monsterTitle}ს", Toast.LENGTH_LONG).show()
    }

    private fun fillDummyData() {
        monstersDummyData.add(MonsterModel(R.drawable.ic_monster_first,"ჯიმშერი","ჯიმშერას უყვარს ჩუსტიკების ჭამა."))
        monstersDummyData.add(MonsterModel(R.drawable.ic_monster_second,"ტურბო გია","ტურბო ისე უკრავს ფანდურზე, თითებს ჩაიკვნიტავ."))
        monstersDummyData.add(MonsterModel(R.drawable.ic_monster_third,"მზექალა","მზექალა მთვარეს სხივებს აძლევს უსასყიდლოდ."))
        monstersDummyData.add(MonsterModel(R.drawable.ic_monster_fourth,"მურტალო","თავისუფალი კაცია, აკეთებს რასაც უნდა და როცა უნდა."))
        monstersDummyData.add(MonsterModel(R.drawable.ic_monster_fifth,"კიჭა","უყვარს ალკოჰოლური სასმელები, განსაკუთრებით ბუმი."))
        monstersDummyData.add(MonsterModel(R.drawable.ic_monster_sixth,"ნათია","ნათია სულ ესეთი კი არ იყო, ცხოვრებამ მოიტანა."))
        monstersDummyData.add(MonsterModel(R.drawable.ic_monster_seventh,"ავთო","ავთოს მესამე რქა ცოლმა დაადგა."))
        monstersDummyData.add(MonsterModel(R.drawable.ic_monster_eith,"ღამურა","უბრალოდ მამამი დრაკულა, COVID-19-იჩი"))
        monstersDummyData.add(MonsterModel(R.drawable.ic_monster_nineth,"ძმები","ბედისგან ხინკლის კუჭივით მიტოვებული ძმები."))
        monstersDummyData.add(MonsterModel(R.drawable.ic_monster_tenth,"დევი","უყვარს წყალი და ნაცარქექიები."))
    }
}