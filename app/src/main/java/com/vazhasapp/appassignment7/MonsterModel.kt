package com.vazhasapp.appassignment7

data class MonsterModel(
    val monsterIcon: Int,
    val monsterTitle: String,
    val monsterDescription: String,
)
