package com.vazhasapp.appassignment7

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vazhasapp.appassignment7.databinding.MonsterLayoutBinding

class RecyclerAdapter(private val monstersList: MutableList<MonsterModel>,
                      private val myListener: VazhaClicker
) : RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        return RecyclerViewHolder(MonsterLayoutBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        ))
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        holder.bindViewHolder()
    }

    override fun getItemCount() = monstersList.size

    inner class RecyclerViewHolder(private val binding: MonsterLayoutBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        private lateinit var currentMonster: MonsterModel

        fun bindViewHolder() {
            currentMonster = monstersList[adapterPosition]
            binding.tvMonsterTitle.text = currentMonster.monsterTitle
            binding.tvMonsterDescription.text = currentMonster.monsterDescription
            binding.ivMonsterIcon.setImageResource(currentMonster.monsterIcon)
        }

        init {
            binding.root.setOnClickListener(this)
        }
        override fun onClick(v: View?) {
            val monsterPosition = adapterPosition

            if (monsterPosition != RecyclerView.NO_POSITION) {
                myListener.vazhaClick(monsterPosition, currentMonster)
            }
        }
    }

    interface VazhaClicker {
        fun vazhaClick(position: Int, currentMonster: MonsterModel)
    }
}